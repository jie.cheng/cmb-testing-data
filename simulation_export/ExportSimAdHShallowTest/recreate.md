ExportSimAdHShallow

To recreate test:
File-Open test2D.cmb from testing-data
File-Open AdHShallowWater.crf from testing-data
Switch to Attribute tab
Select "Functions" tab
Click New. Edit (double click) Row 3 to have x=3 f(x)=4
Select "Materials" tab
Click New. Click below scroll bar to associations
Associate Face1 then Face3
Select "Boundary Conditions" tab
Select "Unit Flow BC" from dropdown, click New
Associated Edge 5, then Edge 2
Click the down arrow on the side tabs
Select "Extrusions" tab
Click New, associate Face 3

File-Export Simulation File
Select "CFD Flow"
Change Level to Advanced
In "Select Analysis Type" section, Select "Constituent Transport"
In "Model", select Model A
In "Output Directory", browse superbuild/cmb/build/Source/Testing/Temporary
In "FileName Base", set ExportSimAdHShallow
In "Python script", browse to cmb-testing-data/simulation_workflow/ADH/AdHSurfaceWater.py
Leave "adh_2d_3d Executable" unchecked (need updated code from ERDC)

Export and Stop Recording

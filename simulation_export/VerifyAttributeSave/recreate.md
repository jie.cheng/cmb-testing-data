VerifyAttributeSave

To recreate test:
File-Open test2D.cmb from testing-data
File-Open AdHSurfaceWater.crf from testing-data
Switch to Attribute tab
Select the Materials tab
Click New. Click below scrollbar
Associate Face1, then Face 4
Click above scrollbar, click New
Click below scrollbar
Associate Face3
Select BoundaryConditions Tab
Click New. Click below scrollbar
Associate Edge4, then Edge7

File-Save Simulation
Save to test.bc in the testing-data folder.
 * Note: Manually edit the script afterward to point to testing/Temporary
Select "AdHSurfaceWater.py" from testing-data as "Python Script"
Save and Stop Recording
